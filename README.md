**10.01**
- Morning meeting.
- Explenation how the eBits works. 
- Deciding on commune comunication place.-Discord
- Understand how works Trello
- Start to work with product description tasks.
- Using Google Drive as document share. 

**11.01**
- Morning meeting.
- Working on new product descriptions.
- Learning about the eBits.

**12.01**
- Morning meeting.
- Brainstorming for new project.
- Working on new product descriptions.

**13.01**
- Morning meeting.
- Brainstorming for new project.
- Working on new product descriptions.


**14.01**
- Morning meeting.
- Working on new product descriptions.
- Validating all our ideas and deciding which project to make.-Postbox allerter.
- And make a small research of what components we need.

> 

**17.01**
- Morning meeting.
- Decidind what parts we are going to use for the project.- ESP32 or ESP8266, Ultrasonic sensor, solar panel and battery. 
- Telling and making order for the needed parts from Nikolaj. 


**18.01**
- Morning meeting.
- Working on web-site until the parts get delivered.
- Make research about the parts. 

**19.01**
- Morning meeting.
- Working on web-site until the parts get delivered.
- Make research about the parts. 
- Search new interesting electronic parts for eBits.

**20.01**
- Morning meeting.
- Working on web-site until the parts get delivered.
- Make research about the parts. 
- Search new interesting electronic parts for eBits.
 

**21.01**
- Morning meeting.
- We get the ordered parts for the project. 
- Start working for the project.
> 


**24.01**
- Morning meeting.
- Assembling the parts and testing them if is working good. 
 

**25.01**
- Morning meeting.
- Preparing the code for the module and testing.
 

**26.01**
- Morning meeting.
- Preparing the code for the module and testing.
- Starting to make the case for the module
 

**27.01**
- Morning meeting.
- Adding battery and solar panel to module to make the final test for next day
- Preparing the code for the module and testing.

**28.01**
- Morning meeting.
- The test of the module was bad because the ultrasonic sensor was now good for this project. 
- Finding a new sensor to use.- KY-032 OBSTACLE-DETECT MODULE
> 

**31.01**
- Morning meeting.
- Working on new sensor.


**01.02**
- Morning meeting.
- Having problem with the new sensor and setting up. 


**02.02**
- Morning meeting. - Making more research how is working the sensor. 
- And testing if is working as intended


**03.02**
- Morning meeting.
- Finding out how is working the sensor and making the new code for it.
- Testing the sensor with new code.


**04.02**
- Morning meeting. 
- Making the final test for new sensor and it was working successfully.
> 


**07.02**
- Morning meeting.
- Change the sensor to KY-033 TRACKING SENSOR because KY-032 OBSTACLE-DETECT MODULE is not easy to use for a begginer and not that precise.
- Search about the new KY-033 TRACKING SENSOR.


**08.02**
- Morning meeting.
- Preparing the code for new sensor.
- Making test of new sensor.
- Making the case for the new parts.

**09.02**
- Morning meeting.
- Final tests of the module. 
- Making the documentation about it. 
- Photos and video of it. 


**10.02**
- Morning meeting.
 

**11.02**
- Morning meeting. 
